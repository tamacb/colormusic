import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';

void main() => runApp(MusicColor());

class MusicColor extends StatelessWidget {

  void Playing(int buttonSound) {
    final player = AudioCache();
    player.play('press$buttonSound.wav');
  }

  Expanded buildKey({Color colors, int soundNumber}) {
    return Expanded(
      child: FlatButton(
        color: colors,
        onPressed: () {
          Playing(soundNumber);
          print('click music press$soundNumber');
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            buildKey(colors: Colors.yellow, soundNumber: 1),
            buildKey(colors: Colors.orange, soundNumber: 2),
            buildKey(colors: Colors.deepOrangeAccent, soundNumber: 3),
            buildKey(colors: Colors.orangeAccent, soundNumber: 4),
            buildKey(colors: Colors.deepOrange, soundNumber: 5),
          ],
        )),
      ),
    );
  }
}
